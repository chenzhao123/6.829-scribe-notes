\documentclass{6046}

\usepackage{tikz}
\usetikzlibrary{arrows}

\author{Kerry Xing, Henry Yuen}
\problemset{2}

\begin{document}
\section{Congestion Control Mechanism in TCP Reno}

In the first part of this lecture, we're going to talk about the congestion
control mechanism in the protocol TCP Reno. First, let's pin down a few
definitions. \textbf{Congestion} of a network is when the amount of packets
entering a network exceeds the number going out. The \textbf{throughput} of a
network is the number of packets (or bytes) per second that arrive at a
receiver. 

The neat thing about the congestion control algorithm in TCP Reno is that it
solves this resource management problem in a completely distributed way: the
solution is implemented via the endpoint behaviors. Additionally, it has the
great property that it can operate well in a wide range of bandwidths, from
hundreds of bits per second to Gigabits per second. 

Let's now talk about our model of the network. It's a graph with two types of
vertices: end-hosts (usually represented by boxes in diagrams), and routers
(usually represented by circles). The edges indicate the connections between
hosts and routers. The routers have \textbf{buffers} to store packets in, that
operate in a FIFO (first-in, first-out) manner. The end-hosts have
\textbf{congestion windows}, which determines how many packets from the
end-host can be ``in the wild'' at any time. 

\begin{tikzpicture}
  % Routers.
  \node (ra) at (0, 0) [circle, draw] {};
  \node (rb) at (1, 0) [circle, draw] {};
  \node (rc) at (2, 0) [circle, draw] {};
  \node (rd) at (3, -1) [circle, draw] {};
  \node (re) at (4, -1) [circle, draw] {};
  \node (rf) at (-1, -1) [circle, draw] {};
  \node (rg) at (-2, -0.5) [circle, draw] {};
  \node (rh) at (-3, -1.5) [circle, draw] {};

  % End-hosts.
  \node (ha) at (-1.5, 1) [rectangle, draw] {};
  \node (hb) at (-2.5, 0.5) [rectangle, draw] {};
  \node (hc) at (-4, -2) [rectangle, draw] {};
  \node (hd) at (-1.5, -2) [rectangle, draw] {};
  \node (he) at (-0.5, -2) [rectangle, draw] {};
  \node (hf) at (3, 1) [rectangle, draw] {};
  \node (hg) at (3.5, -2) [rectangle, draw] {};
  \node (hh) at (5, -0.5) [rectangle, draw] {};
  \node (hi) at (5, -1) [rectangle, draw] {};
  \node (hj) at (5, -1.5) [rectangle, draw] {};

  % Connections between routers.
  \draw (ra) -- (rb);
  \draw (rb) -- (rc);
  \draw (rc) -- (rd);
  \draw (rd) -- (re);
  \draw (ra) -- (rf);
  \draw (rf) -- (rg);
  \draw (rg) -- (rh);

  % Connections between routers and end-hosts.
  \draw (ra) -- (ha);
  \draw (rg) -- (hb);
  \draw (rh) -- (hc);
  \draw (rf) -- (hd);
  \draw (rf) -- (he);
  \draw (rc) -- (hf);
  \draw (rd) -- (hg);
  \draw (re) -- (hh);
  \draw (re) -- (hi);
  \draw (re) -- (hj);

  % Buffer picture.
  \draw[xstep=0.1, ystep=0.5] (-0.51, 0.5) grid (0.5, 1);
  \node at (0, 1.5) {(buffer)};
\end{tikzpicture}

One of the network behaviors we want to avoid is \textbf{congestion collapse}.
This phenomenon can be described in the following manner: plot the network
throughput against the offered load (how many packets are being put into the
network) over time. Ideally, we would like to see the curve rise monotonically,
and then plateau as the throughput reaches maximum capacity. However, this
curve may dip precipitously; this is congestion collapse.

\begin{tikzpicture}
  % Draw the grid.
  %\draw[help lines] (0, 0) grid (7, 6);

  % Axes.
  \draw[-triangle 45, auto] (0, 0) -- node[swap]{Offered load} (7.5, 0);
  \draw[-triangle 45, auto] (0, 0) -- node{$\lambda$: throughput} (0, 6.5);

  % Draw the graph.
  \draw (0, 0) -- node[sloped, above]{underutilized} (2, 4);
  \fill (2, 4) circle(2pt);
  \draw (2, 4) to[out=63, in=185] node[sloped, above]{queueing} (6, 5);
  \fill (6, 5) circle(2pt);
  \draw (6, 5) to[out=0, in=120] (7, 2);
  \node at (8.5, 4) {congestion collapse};
\end{tikzpicture}

Throughout, we usually denote throughput with the symbol $\lambda$. One measure
of network goodness is
$\log \frac{\lambda}{\mathrm{Delay}^\alpha}
= \log\lambda - \alpha \log \mathrm{Delay}$.
Modern congestion control algorithms try to maximize this number.

TCP Reno tries to minimize these congestion collapse events. It operates in two
phases: there is the ``slow-start'' phase, and then the AIMD (Additive
increase/multiplicative decrease) phase, also termed ``congestion avoidance''. 

\section{Self clocking}

An important property of the network that TCP Reno relies on is
\textbf{self clocking}. This allows end hosts to time when to send packets
without actually using an explicit timer. Instead, they rely on the reception of
ACKs to trigger sending (or to perform other actions such as to scale down the
congestion window). Consider the following example: suppose a router suddenly
experiences many more connections. The end hosts that have been previously
connected to the router will realize that the network is more crowded, simply
because the ACKs for their packets is taking longer to arrive, and thus the
senders will naturally slow down.

Note: For pedagogical purposes, we'll measure data in packets, we'll assume
that all packets have the same size, and that all packets have a sequence
number (technically not true, but will simplify the discussion). 

In TCP Reno, the specification for ACKs is the
\textbf{Cumulative acknowledgment policy}. Each ACK has a sequence number
corresponding to the packet that it's acknowledging. If the received ACK is $x$;
then the assumption is that the receiver has also received all packets up to
$x - 1$.

Self-clocking is implemented in TCP Reno via the variable $cwnd$. This variable
determines how many packets the sender will try to send in one unit of time. The
problem of congestion control boils down to: how do we adjust $cwnd$?

TCP protocols usually set $cwnd$ to a small initial value, such as $1$ or $2$.
Then, we will undergo the \textbf{slow-start} phase.

Note: usually, the sending window of a sender is actually set to the minimum of
$cwnd$ and the receiver's advertised window, but for now we'll assume that the
latter is infinity, and will just assume the sending window is $cwnd$ exactly.

\section{Slow-start}

The slow-start is actually a rather fast start. The algorithm works as follows:
upon reception of every ACK, $cwnd$ is incremented by $1$. This causes a
exponential increase in $cwnd$ by time. When this exponential increase in
$cwnd$ hits network capacity, packets must start dropping, and ACKs are not
being received. This signals to the sender that there is network congestion
\footnote{This is an important feature in these self-clocking protocols: the
only signal for network congestion to the end hosts is packet loss.}. 

When this signal is received, then TCP Reno will cut $cwnd$ in half, and then
goes into ``congestion avoidance'' mode, where $cwnd$ will increase in a much
more conservative fashion.

\section{Congestion avoidance}

In this mode, we have the following rule:
\begin{itemize}
  \item \textbf{If no congestion is detected}. \emph{Additive increase rule}:
      $cwnd \leftarrow cwnd + \alpha$ each round trip (not each ACK).
  \item \textbf{If congestion is detected}. \emph{Multiplicative decrease rule}:
      $cwnd \leftarrow (1-\beta) cwnd$.
\end{itemize}

Here, we usually set $\alpha = 1$. $\beta$ is usually set to $1/2$.

The analysis of this algorithm can be found in Chin-Jain. They showed that this
algorithm leads to fair allocation of network resources. We give a sketch of an
argument for why. Imagine we're in the scenario where there are two end hosts
connected to the same network. The hosts have throughputs $\lambda_1$ and
$\lambda_2$, respectively. Observe that the throughput of each host $i$ is
$\lambda_i = cwnd_i/rtt_i$.

\begin{tikzpicture}
  % Draw the grid.
  %\draw[help lines] (0, 0) grid (6, 6);

  % Axes.
  \draw[-triangle 45, auto] (0, 0) -- node[swap]{$\lambda_1$} (6.5, 0);
  \draw[-triangle 45, auto] (0, 0) -- node{$\lambda_2$} (0, 6.5);

  % Draw the lambda1 + lambda2 = C line.
  \draw[triangle 45-triangle 45] (-0.5, 6.5) -- (6.5, -0.5);
  \node at (7, 0.5) {$\lambda_1 + \lambda_2 = C$};
  % Draw the lambda1 = lambda2 line.
  \draw[triangle 45-triangle 45, style=dashed] (-0.5, -0.5) -- (6.5, 6.5);
  \node at (7, 6) {$\lambda_1 = \lambda2$};

  % Plot a graph.
  \fill (6, 2) circle(2pt);
  \draw[-triangle 45] (6, 2) -- (3, 1);
  \fill (3, 1) circle(1pt);
  \draw[-triangle 45] (3, 1) -- (4.5, 2.5);
  \fill (4.5, 2.5) circle(1pt);
  \draw[-triangle 45] (4.5, 2.5) -- (2.25, 1.25);
  \fill (2.25, 1.25) circle(1pt);
  \draw[-triangle 45] (2.25, 1.25) -- (4, 3);
  \fill (4, 3) circle(1pt);
  \draw[-triangle 45] (4, 3) -- (2, 1.5);
  \fill (2, 1.5) circle(1pt);
  % Eventual convergence.
  \draw[-triangle 45, style=dashed] (2, 1.5) -- (3, 3);
  \draw (3, 3) circle(2pt);
\end{tikzpicture}

We can visualize the dynamics of the network utilization by plotting a curve
that represents how $\lambda_1,\lambda_2$ change over time on a two-dimension
graph with the vertical axis $\lambda_2$ and the horizontal axis $\lambda_1$.
Let $C$ be the capacity of the network. The contour curve
$\lambda_1 + \lambda_2 = C$ represents maximum usage of the network by the two
hosts. The curve $\lambda_1 = \lambda_2$ represents fair division of network
resources between the two. The ideal location we'd like to be is their
intersection, the point where $\lambda_1 = \lambda_2 = C/2$. The dynamics of the
congestion avoidance phase of TCP Reno is to naturally gravitate towards this
ideal point.

Suppose that congestion is detected. This implies that our network utilization
is a point beyond the $\lambda_1 + \lambda_2 = C$ curve. The multiplicative
decrease rule is implemented for both parties (we're assuming synchronized
notification of packet losses to both hosts). This decrease rule happens until
we're no longer experiencing congestion. At this point,
$\lambda_1 + \lambda_2 \leq C$, and the additive increase rule kicks in.
However, this will push $(\lambda_1,\lambda_2)$ to move parallel to the
$\lambda_1 = \lambda_2$ direction. But because of the multiplicative decrease
rule, we've decreased closer to origin when there was congestion; after a
series of additive increases and multiplicative decreases, it can be shown that
we will converge to fair and optimal allocation of network resources, i.e.
$\lambda_1 = \lambda_2 = C/2$.

This behavior only happens when you have the combination additive
increase/multiplicative decrease. For example, the additive increase/additive
decrease policy doesn't do anything useful, because we simply sawtooth back and
forth between congestion and not congestion. 

Note that the policy that gets you closer to fairness is the additive increase,
because the ratio between the end hosts' $\lambda$'s is converging to $1$.

\section{TCP Compatibility (TCP Friendliness)}

Many companies sold web-accelerating prowlers, which were designed to cheat
TCP, so that the users would get higher throughput, at the expense of other
users. These web-accelerating prowlers caused a lot of problems with honesty
over the network, so methods were developed to see if people were cheating TCP.
(Side note: Cheating TCP implies sending more packets than is reasonable when
the network is congested. It is not considered cheating to take up more
bandwidth if the network is underutilized.)
In other words, people were checking to see if other users were using
\textbf{TCP Compatible} protocols, or protocols that exhibit the same network
behavior as TCP.

\begin{lemma}
The throughput for TCP-Reno in the congestion control state,
$\lambda_{Reno}$ (in bytes per second), is upper-bounded by
$$ \lambda_{Reno} \le \frac{c \cdot s}{rtt \cdot \sqrt{l}} $$
where $c$ is a constant, $s$ is the packet size, and $l$ is the packet loss rate.
\end{lemma}
\begin{proof}

\begin{tikzpicture}
  % Draw the grid.
  %\draw[help lines] (0, 0) grid (12, 5);

  % Axes.
  \draw[-triangle 45, auto] (0, 0) -- node[swap]{Time} (12.5, 0);
  \draw[-triangle 45, auto] (0, 0) -- node{cwnd} (0, 5.5);

  % Plot a graph.
  \fill (0, 0) circle(2pt);
  \draw[domain=0:2] plot(\x, \x * \x);
  \fill (2, 4) circle(2pt);
  \draw (2, 4) -- (2, 2);
  \fill (2, 2) circle(2pt);
  \draw (2, 2) -- (4, 3);
  \fill (4, 3) circle(2pt);
  \draw (4, 3) -- (4, 1.5);
  \fill (4, 1.5) circle(2pt);
  \draw (4, 1.5) -- (7, 2.75);
  \fill (7, 2.75) circle(2pt);
  \draw (7, 2.75) -- (7, 1.375);
  \fill (7, 1.375) circle(2pt);
  \draw (7, 1.375) -- (11, 3.375);
  \fill (11, 3.375) circle(2pt);
  \draw (11, 3.375) -- (11, 1.6875);
  \fill (11, 1.6875) circle(2pt);
  \draw (11, 1.6875) -- (12, 2.1875);
\end{tikzpicture}

In a normal period of TCP-Reno congestion control, the congestion window will
grow from $\frac{W_{max}}{2}$ to $W_{max}$, in increments of 1 (since the cwnd
increases by 1 for each RTT). Then, a packet will drop, because of too many
packets in the network. Thus, the number of packets sent in this period
is:
\begin{eqnarray*}
& & \frac{W_{max}}{2} + \paren{\frac{W_{max}}{2} + 1} + \hdots +
\paren{\frac{W_{max}}{2} + \frac{W_{max}}{2}} \\
&=& \paren{\frac{W_{max}}{2} + \frac{W_{max}}{2} + \hdots + \frac{W_{max}}{2}}
+ \paren{1 + 2 + \hdots + \frac{W_{max}}{2}} \\
&\approx& \frac{W_{max}^2}{4} + \frac{W_{max}^2}{8} + O\paren{W_{max}}
\end{eqnarray*}
Since one packet dropped in this time, we get:
\begin{equation} \label{eq:l_result}
l^{-1} = \frac{3}{8} w_{max}^2
\end{equation}
Note that we have been a bit sloppy in our calculations, because the RTT will
increase as the network gets more congested, due to the queueing delays.
However, since the result we obtain will be an over-estimate, which is fine for
an upper bound.

Since the average throughput is the average window size divided by the round
trip time, we get
$$ \frac{W_{max}}{2~rtt} \le \lambda_{Reno} \le \frac{3W_{max}}{4~rtt} $$
since the window size at any given moment is at least $\frac{W_{max}}{2}$
and the average window size over the period is $\frac{3W_{max}}{4}$.
Using the (\ref{eq:l_result}), we get
$$ \lambda_{Reno} \le \frac{c}{rtt \sqrt{l}} $$
However, this result is in packets per second, so to convert to bytes per
second, we multiply by the packet size, $s$:
$$ \lambda_{Reno} \le \frac{c s}{rtt \sqrt{l}} $$
\end{proof}

Side note: One can derive that $\lambda \sim \frac{1}{l}$ during the
slow-start phase.

\section{Paths of Research}
Research took off in many directions after the introduction of TCP.
Four of them were:
\begin{enumerate}
\item High-speed TCP (TCP performs poorly at the scale of high-speed TCP
networks.)
\item Loss dependence (Delay based congestion control, such as TCP Vegas)
\item Video (Since video quality should not oscillate, the congestion control
mechanism should be slowly responsive. Equation Based Congestion Control (EBCC))
\item Wireless (Wireless networks have a large bit error rate, so a packet
drop is not a good indication of congestion.)
\end{enumerate}

\section{Delay Based Congestion Control}
The idea of \textbf{delay based congestion control} is to use RTT measurements
to tell how much of the network is utilized. Originally, the idea was to use
increasing RTT measurements as a sign that the network is getting congested,
and decrease cwnd. However, this idea was criticized, because the
self-clocking mechanism already slows down the sending of packets as the RTT
increases.

Ironically, the idea of delay based congestion control turned out to be a good
idea because RTTs can be used to reach link capacity faster than TCP!

Delay based congestion control works by setting a base RTT, $rtt_{min}$. Then,
given the window size, $wnd$, the throughput is calculated:
$$ \lambda = \frac{wnd}{rtt} $$
The expected throughput is also calculated:
$$ \lambda_{expected} = \frac{wnd}{rtt_{min}} $$
Note that expected throughput is always higher than the actual throughput, since
the expected throughput uses $rtt_{min}$.

The difference between expected throughput and actual throughput changes the
window size:
$$ Diff = \lambda_{expected} - \lambda \ge 0 $$

The usage of $Diff$ depends on the protocol:

\subsection{TCP Vegas}
TCP Vegas chooses two parameters $\alpha$ and $\beta$, and changes the window
size depending on the value of $Diff$.
$$
\cases{
\cif{Diff \le \alpha}: & \text{Increase window linearly} \\
\cif{Diff \ge \beta}: & \text{Decrease window linearly}
}
$$

\subsection{Microsoft}
Microsoft bases its window size on two parameters. The first parameter, $cwnd$,
is chosen as in TCP Reno. The second parameter $dwnd$, changes depending on the
value of $Diff$.
$$
\cases{
\cif{Diff < \gamma}: & dwnd \leftarrow dwnd + \frac{1}{win^k} \\
\cif{Diff > \gamma}: & dwnd \leftarrow dwnd - f(win^l)
}
$$
for some function $f$, and chosen parameters $k$ and $l$. The total window size
is the sum of the two windows: $wnd = cwnd + dwnd$. Interestingly, if
$k + l = 1$, we will get a TCP friendly congestion control scheme.

\end{document}

